
const pathToEncountersList = "/encountersToClear"

skippedBecauseNoEncounter = [];
encountersWithNoClaims = [];
encountersWithMultipleClaims = [];
encountersMarkedAsBilledInitially = [];

// Cleans up a string in the case where it might be undefined
function str(str) {
  if(str === undefined) {
    return UNDEFINED_STR;
  } else {
    return str;
  }
}

const encounterIdsToClear = JSON.parse(cat(pathToEncountersList).replace(/\n/g, ""));
encounterIdsToClear.forEach(encounterId => {
  const encountersCursor = db.getCollection("healthCoachEncounter").find({"_id": ObjectId(encounterId)});
  if(!encountersCursor.hasNext()) {
    print("Could not find encounter with id " + encounterId);
    skippedBecauseNoEncounter.push(encounterId);
    return;
  }
  const encounter = encountersCursor.next();
  if(encounter.billed) {
    encountersMarkedAsBilledInitially.push(encounterId);
  }
  db.getCollection("healthCoachEncounter").update({"_id":ObjectId(encounterId)}, {$set:{"billed": false}});
  const claimCursor = db.getCollection("claim").find({"encounterId":encounterId});
  if(!claimCursor.hasNext()) {
    encountersWithNoClaims.push(encounterId);
    return;
  }
  if(claimCursor.count() > 1) {
    encountersWithMultipleClaims.push(encounterId);
  }
  claimsToRemove = [];
  claimCursor.forEach(claim => {
    print("Encounter " + encounterId + " has matching claim with claim id " + str(claim._id.str) + ". This claim is being deleted now.");
    claimsToRemove.push(claim);
  });
  claimsToRemove.forEach(claim => {
    db.getCollection("claim").remove({"_id":claim._id});
  });
});

print("Skipped encounters because couldn't find in database: " + JSON.stringify(skippedBecauseNoEncounter));
print("Encounters with no matching claim: " + JSON.stringify(encountersWithNoClaims));
print("Encounters with multiple (more than one) claims: " + JSON.stringify(encountersWithMultipleClaims));
print("Encounters that were initially marked as billed: " + JSON.stringify(encountersMarkedAsBilledInitially));
