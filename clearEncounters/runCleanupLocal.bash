#! /bin/bash

#Either
docker cp cleanup.js mongodb:/cleanupEncounters.js
docker cp encountersToClear mongodb:/encountersToClear

#Remote only
#docker exec mongodb sh -c "mongo creo-db --authenticationDatabase admin -u root -p 1bbWlQ7V7IRgCnm --host mongo1.creochange.net --ssl < /cleanupEncounters.js"

#Local (test) only
docker exec mongodb sh -c "mongo 'localhost:27017' < /cleanupEncounters.js"
